package elevator

import (
	"testing"
	"time"
)

func TestInstanceCreation(t *testing.T) {
	// Default values should be initialized
	t.Run("Init initializes default values", func(t *testing.T) {
		Init()
		if controlUnit[DEFAULT_PASSENGER_SYSTEM] == nil {
			t.Errorf("Control unit not initialized")
		}
		if len(controlUnit[DEFAULT_PASSENGER_SYSTEM].Elevators) == 0 {
			t.Errorf("At least one elevator should have been initialized")
		}
	})

	// Should return true if add elevator succeeds, and false if type is not present
	t.Run("Add elevator adds an elevator if type is present", func(t *testing.T) {
		initLength := len(controlUnit[DEFAULT_PASSENGER_SYSTEM].Elevators)
		if !AddElevator(DEFAULT_PASSENGER_SYSTEM) {
			t.Errorf("Failed to add elevator")
		}
		if (initLength + 1) != len(controlUnit[DEFAULT_PASSENGER_SYSTEM].Elevators) {
			t.Errorf("Elevator count mismatch")
		}
		if AddElevator("arbitrary_string_for_failure") {
			t.Errorf("Unknown type elevator addition should fail")
		}
	})

	// Should add new type
	t.Run("Adds a new type if not present", func(t *testing.T) {
		arbitraryString := "DEMOSYS"
		AddType(arbitraryString)
		if controlUnit[arbitraryString] == nil {
			t.Errorf("Type should have been added")
		}
	})

	// Test floor panel creation
	t.Run("Adds a new floor panel", func(t *testing.T) {
		controlSystem := controlUnit[DEFAULT_PASSENGER_SYSTEM]
		fpLen := 0
		for range controlSystem.FpMapper {
			fpLen++
		}
		AddFloorPanel(controlSystem.Type, 1)
		fpLenNew := 0
		for range controlSystem.FpMapper {
			fpLenNew++
		}
		if fpLen != (fpLenNew - 1) {
			t.Errorf("Inconsistent length")
		}
		if controlSystem.FpMapper[1] == nil {
			t.Errorf("Floor panel initialization failed")
		}
	})
}

func TestElevatorOperation(t *testing.T) {
	// If there are idle elevators, requests will be immediately processed
	t.Run("process request immediately if idle elevator is present", func(t *testing.T) {
		controlSystem := controlUnit[DEFAULT_PASSENGER_SYSTEM]
		AddFloorPanel(controlSystem.Type, 2)
		fp := controlSystem.GetFloorPanel(2)
		currElevator := controlSystem.GetElevator(0)
		fp.CallElevator(false)
		time.Sleep(5 * time.Second)
		if len(currElevator.Stops) != 1 || len(controlSystem.RequestQueue) != 0 {
			t.Errorf("Request wasn't processed")
		}
	})

	// If there are no idle elevators, requests will be pushed to queue or if possible processed
	t.Run("request pushed to queue if idle elevator is not present", func(t *testing.T) {
		controlSystem := controlUnit[DEFAULT_PASSENGER_SYSTEM]
		offElevator := controlSystem.GetElevator(1)
		offElevator.SwitchOff()
		AddFloorPanel(controlSystem.Type, 3)
		fp := controlSystem.GetFloorPanel(1)
		fp.CallElevator(true)
		time.Sleep(time.Second)
		if len(controlSystem.RequestQueue) != 1 {
			t.Errorf("Request wasn't queued")
		}
	})

	// Whenever elevators, get free request will be pushed from queue to elevator
	t.Run("idle elevators should take up queued requests, and should visit given floor", func(t *testing.T) {
		controlSystem := controlUnit[DEFAULT_PASSENGER_SYSTEM]
		offElevator := controlSystem.GetElevator(1)
		go offElevator.SwitchOn()
		time.Sleep(time.Second)
		if offElevator.Status != STATUS_GOING_UP {
			t.Errorf("Processing failed")
		}
		time.Sleep(offElevator.Interval)
		if offElevator.CurrentFloor != 1 {
			t.Errorf("queued request not taken up")
		}
	})

	// Elevator panel requests should get satisfied
	t.Run("button inside elevator should lead to desired floor when pressed", func(t *testing.T) {
		controlSystem := controlUnit[DEFAULT_PASSENGER_SYSTEM]
		testElevator := controlSystem.GetElevator(1)
		go testElevator.PressFloorButton(3)
		time.Sleep(time.Second)
		if testElevator.Status != STATUS_GOING_UP {
			t.Errorf("Processing failed")
		}
		time.Sleep(4 * testElevator.Interval)
		if testElevator.CurrentFloor != 3 {
			t.Errorf("queued request not taken up")
		}
	})

	// Elevator should queue request when up request from floor panel is made but elevator is going down & vice versa
	t.Run("request queued when up request is made from floor panel but elevator is going down & vice versa", func(t *testing.T) {
		controlSystem := controlUnit[DEFAULT_PASSENGER_SYSTEM]
		testElevator := controlSystem.GetElevator(1)
		offElevator := controlSystem.GetElevator(0)
		offElevator.SwitchOff()
		testElevator.PressFloorButton(1)
		fp := controlSystem.GetFloorPanel(2)
		queueLenInit := len(controlSystem.RequestQueue)
		fp.CallElevator(true)
		queueLenFinal := len(controlSystem.RequestQueue)
		if queueLenInit != (queueLenFinal - 1) {
			t.Errorf("Unexpected queue length")
		}
		controlSystem.RequestQueue = []Request{}
		time.Sleep(4 * testElevator.Interval)
		testElevator.PressFloorButton(3)
		fp = controlSystem.GetFloorPanel(2)
		queueLenInit = len(controlSystem.RequestQueue)
		fp.CallElevator(false)
		queueLenFinal = len(controlSystem.RequestQueue)
		if queueLenInit != (queueLenFinal - 1) {
			t.Errorf("Unexpected queue length")
		}
		controlSystem.RequestQueue = []Request{}
	})

	// Elevator is going up, but request to go up is made from floor below elevator floor
	t.Run("request queued when elevator is going up, but request to go up is made from floor below & vice versa for going down", func(t *testing.T) {
		controlSystem := controlUnit[DEFAULT_PASSENGER_SYSTEM]
		AddFloorPanel(controlSystem.Type, 4)
		testElevator := controlSystem.GetElevator(1)
		time.Sleep(4 * testElevator.Interval)
		go testElevator.PressFloorButton(1)
		time.Sleep(time.Second)
		fp := controlSystem.GetFloorPanel(4)
		queueLenInit := len(controlSystem.RequestQueue)
		go fp.CallElevator(false)
		time.Sleep(time.Second)
		queueLenFinal := len(controlSystem.RequestQueue)
		if queueLenInit != (queueLenFinal - 1) {
			t.Errorf("Unexpected queue length")
		}
		controlSystem.RequestQueue = []Request{}
		go testElevator.PressFloorButton(2)
		time.Sleep(2 * testElevator.Interval)
		go testElevator.PressFloorButton(4)
		time.Sleep(time.Second)
		fp = controlSystem.GetFloorPanel(1)
		queueLenInit = len(controlSystem.RequestQueue)
		go fp.CallElevator(true)
		time.Sleep(time.Second)
		queueLenFinal = len(controlSystem.RequestQueue)
		if queueLenInit != (queueLenFinal - 1) {
			t.Errorf("Unexpected queue length")
		}
		controlSystem.RequestQueue = []Request{}
	})

	// Immediate processing
	// Elevator is going down, request for down is made from floor below elevator floor
	// Elevator is going up, request to go up is made from floor above elevator floor
	t.Run("requests processed immediately when direction is same", func(t *testing.T) {
		controlSystem := controlUnit[DEFAULT_PASSENGER_SYSTEM]
		AddFloorPanel(controlSystem.Type, 4)
		testElevator := controlSystem.GetElevator(1)
		time.Sleep(4 * testElevator.Interval)
		go testElevator.PressFloorButton(4)
		time.Sleep(time.Second)
		qLenInit := len(controlSystem.RequestQueue)
		fp := controlSystem.GetFloorPanel(3)
		fp.CallElevator(true)
		time.Sleep(time.Second)
		qLenFinal := len(controlSystem.RequestQueue)
		if qLenFinal != qLenInit {
			t.Errorf("Unexpected queue length")
		}
		time.Sleep(5 * testElevator.Interval)
		go testElevator.PressFloorButton(1)
		time.Sleep(time.Second)
		qLenInit = len(controlSystem.RequestQueue)
		fp = controlSystem.GetFloorPanel(3)
		fp.CallElevator(false)
		time.Sleep(time.Second)
		qLenFinal = len(controlSystem.RequestQueue)
		if qLenFinal != qLenInit {
			t.Errorf("Unexpected queue length")
		}
	})
}
