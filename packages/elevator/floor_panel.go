package elevator

import "fmt"

type FloorPanel struct {
	Floor       uint
	InvokedUp   bool
	InvokedDown bool
	System      *ElevatorSystem
}

type Request struct {
	Floor uint
	Up    bool
	Fp    *FloorPanel
}

/**
For floor panel we don't need any mutexes, since for
the invoked up and down buttons we need the latest value anyways
*/

func (fp *FloorPanel) CallElevator(up bool) {
	if up {
		fp.InvokedUp = true
	} else {
		fp.InvokedDown = true
	}
	fp.System.ProcessRequest(Request{Up: up, Floor: fp.Floor, Fp: fp})
}

func (fp *FloorPanel) FulfillRequest(up bool) {
	if up {
		fp.InvokedUp = false
	} else {
		fp.InvokedDown = false
	}
}

// Add Floor Panel
func AddFloorPanel(typ string, floor uint) bool {
	if controlUnit[typ] == nil {
		fmt.Println("Failed to add floor panel, as control unit is not present")
		return false
	}
	sys := controlUnit[typ]
	// Lock to ensure only one floorPanel of a specific type and unique floor number can be added at a time
	sys.mu.Lock()
	defer sys.mu.Unlock()
	if sys.FpMapper[floor] != nil {
		fmt.Println("Floor panel already exists")
		return false
	}
	floorPanel := &FloorPanel{
		Floor:  floor,
		System: sys,
	}
	if floor > sys.MaxFloor {
		sys.MaxFloor = floor
	}
	if floor < sys.MinFloor {
		sys.MinFloor = floor
	}
	sys.FpMapper[floor] = floorPanel
	fmt.Println("Floor panel added successfully")
	return true
}
