package elevator

import (
	"fmt"
	"sort"
	"sync"
	"time"
)

const (
	STATUS_IDLE       = 0
	STATUS_GOING_UP   = 100
	STATUS_GOING_DOWN = 200
)

type Elevator struct {
	mu              sync.Mutex
	ID              uint
	Status          uint
	CurrentFloor    uint
	Stops           []uint
	DoorOpen        bool
	KeepDoorOpen    chan bool
	HoldDoor        bool
	StartProcessing chan bool
	DoorOpenedAt    time.Time
	OpenTime        time.Duration
	Interval        time.Duration
	On              bool
	System          *ElevatorSystem
}

// Add elevator of specific type
func AddElevator(typ string) bool {
	if controlUnit[typ] == nil {
		fmt.Printf("%s type doesn't exist, create type first\n", typ)
		return false
	}
	typControl := controlUnit[typ]
	// Lock to ensure only one elevator of a specific type can be added at a time
	// so as to get an unique id = count of elevators
	typControl.mu.Lock()
	elevator := &Elevator{
		ID:              uint(len(typControl.Elevators)),
		Status:          STATUS_IDLE,
		CurrentFloor:    0,
		Stops:           []uint{},
		Interval:        DEFAULT_INTERVAL,
		OpenTime:        DEFAULT_DOOR_OPEN,
		KeepDoorOpen:    make(chan bool),
		StartProcessing: make(chan bool),
		On:              false,
		System:          typControl,
	}
	typControl.Elevators = append(typControl.Elevators, elevator)
	typControl.mu.Unlock()
	go elevator.SwitchOn()
	time.Sleep(time.Second)
	return true
}

// Check if elevator can go to a particular floor presently
func (e *Elevator) canGoTo(floor uint) bool {
	if e.Status == STATUS_GOING_DOWN && floor >= e.CurrentFloor {
		return false
	}
	if e.Status == STATUS_GOING_UP && floor <= e.CurrentFloor {
		return false
	}
	return true
}

// Check if elevator can accept a request
func (e *Elevator) CanHonorFloorPanelRequest(r Request) bool {
	canGo := e.canGoTo(r.Floor)
	if !canGo {
		return false
	}
	if e.Status == STATUS_IDLE {
		return true
	}
	if r.Up {
		if e.Status == STATUS_GOING_DOWN {
			return false
		} else {
			return e.canGoTo(r.Floor)
		}
	} else {
		if e.Status == STATUS_GOING_UP {
			return false
		} else {
			return e.canGoTo(r.Floor)
		}
	}
}

// Close door after timeout
func (e *Elevator) closeDoorAfterTimeout() {
	time.Sleep(e.OpenTime)
	if e.DoorOpen {
		e.KeepDoorOpen <- false
	}
}

// Process request and add stop
/**
Why do we need a lock ?
We always need Stops to be sorted, on concurrent requests from
multiple floors, we might have data inconsistency
This function is invoked by control unit, when it gets signal from a floor panel
*/
func (e *Elevator) AddFPRequest(r Request) bool {
	if e.CanHonorFloorPanelRequest(r) {
		e.AddStop(r.Floor)
		return true
	}
	return false
}

func (e *Elevator) AddStop(f uint) {
	fmt.Println("Adding stop: ", f)
	e.mu.Lock()
	e.Stops = append(e.Stops, f)
	e.mu.Unlock()
	lastStatus := e.Status
	if len(e.Stops) == 1 {
		if e.CurrentFloor < f {
			e.mu.Lock()
			defer e.mu.Unlock()
			e.Status = STATUS_GOING_UP
			fmt.Printf("Request for elevator(%s) in floor %d will be handled by elevator %d\n", e.System.Type, f, e.ID)
		} else if e.CurrentFloor > f {
			e.mu.Lock()
			defer e.mu.Unlock()
			e.Status = STATUS_GOING_DOWN
			fmt.Printf("Request for elevator(%s) in floor %d will be handled by elevator %d\n", e.System.Type, f, e.ID)
		} else {
			e.mu.Lock()
			defer e.mu.Unlock()
			fmt.Printf("%s Elevator %d Door Opened\n", e.System.Type, e.ID)
			e.DoorOpen = true
			e.DoorOpenedAt = time.Now()
			// Wait for door to be closed, or set a default timer
			go e.closeDoorAfterTimeout()
			val := <-e.KeepDoorOpen
			for e.HoldDoor {
				time.Sleep(time.Second)
			}
			e.Stops = e.Stops[1:]
			fmt.Printf("%s Elevator %d Door Closed\n", e.System.Type, e.ID)
			e.DoorOpen = val
		}
		if lastStatus == STATUS_IDLE {
			e.StartProcessing <- true
		}
	} else {
		if e.Status == STATUS_GOING_DOWN {
			sort.Slice(e.Stops, func(i, j int) bool { return e.Stops[i] > e.Stops[j] })
		} else {
			sort.Slice(e.Stops, func(i, j int) bool { return e.Stops[i] < e.Stops[j] })
		}
	}
}

func (e *Elevator) SwitchOn() {
	fmt.Println("Turning on Elevator ", e.ID)
	e.mu.Lock()
	if e.On {
		fmt.Println("Already on")
		return
	}
	e.On = true
	e.mu.Unlock()
	for e.On {
		if len(e.Stops) > 0 {
			time.Sleep(e.Interval)
			// No mutexes required for modification of current floor, as it is being modified
			// only in a single thread
			if e.Status == STATUS_GOING_UP {
				e.CurrentFloor++
			} else if e.Status == STATUS_GOING_DOWN {
				e.CurrentFloor--
			}
			fmt.Printf("%s Elevator %d is currently at %d\n", e.System.Type, e.ID, e.CurrentFloor)
			/**
			Stop on the current floor only if it has a control unit and someone invoked an
			elevator from it
			*/
			if e.System.FloorHasControlUnit(e.CurrentFloor) && (e.Stops[0] == e.CurrentFloor) {
				fmt.Printf("%s Elevator %d Stopped at floor: %d\n", e.System.Type, e.ID, e.CurrentFloor)
				e.mu.Lock()
				e.Stops = e.Stops[1:]
				fmt.Printf("%s Elevator %d Door Opened, Stops Rem.: %v\n", e.System.Type, e.ID, e.Stops)
				e.DoorOpen = true
				e.DoorOpenedAt = time.Now()
				e.mu.Unlock()
				// Mark as processed
				if e.Status == STATUS_GOING_UP {
					e.System.ReqFinished(e.CurrentFloor, true)
				} else {
					e.System.ReqFinished(e.CurrentFloor, false)
				}

				// Wait for door to be closed, or set a default timer
				go e.closeDoorAfterTimeout()
				val := <-e.KeepDoorOpen
				for e.HoldDoor {
					time.Sleep(time.Second)
				}
				fmt.Printf("%s Elevator %d Door Closed\n", e.System.Type, e.ID)
				e.DoorOpen = val
			}
		} else {
			/* When elevator finishes processing requests, change it's status to idle
			and wait for it to receive a start processing signal
			*/
			e.Status = STATUS_IDLE
			fmt.Printf("%s Elevator %d is idle and waiting to process requests\n", e.System.Type, e.ID)
			go e.System.OnIdleElevator(e)
			<-e.StartProcessing
		}
	}
	fmt.Printf("%s Elevator %d shut down\n", e.System.Type, e.ID)
}

// Function to explicitly close door
func (e *Elevator) CloseDoor() {
	e.HoldDoor = false

	// Send channel signal, only if e.OpenTime has not elapsed,
	// else, if we try to write data to closed channel it'll panic
	now := time.Now()
	diff := now.Sub(e.DoorOpenedAt)
	if diff < e.OpenTime {
		e.KeepDoorOpen <- false
	}
}

// code to defer closing of door
func (e *Elevator) SwitchHoldDoor() {
	if e.DoorOpen {
		e.HoldDoor = !e.HoldDoor
		return
	}
	fmt.Println("Door is closed")
}

// Turn off elevator
func (e *Elevator) SwitchOff() {
	if len(e.Stops) > 0 || e.Status != STATUS_IDLE {
		return
	}
	e.mu.Lock()
	e.On = false
	if e.Status == STATUS_IDLE {
		e.StartProcessing <- true
	}
	e.mu.Unlock()
}

// Elevator panel request
func (e *Elevator) PressFloorButton(f uint) {
	if e.canGoTo(f) {
		e.AddStop(f)
	}
}
