package elevator

import (
	"fmt"
	"sync"
)

type ElevatorSystem struct {
	mu           sync.Mutex
	reqMutex     sync.Mutex
	Type         string
	Elevators    []*Elevator
	RequestQueue []Request
	FpMapper     map[uint]*FloorPanel
	MinFloor     uint
	MaxFloor     uint
}

/**
Ask Control unit whether this particular floor has a control panel or not
*/
func (es *ElevatorSystem) FloorHasControlUnit(currentFloor uint) bool {
	return es.FpMapper[currentFloor] != nil
}

/**
Process request
*/
func (es *ElevatorSystem) ProcessRequest(req Request) {
	requestDirection := "up"
	if !req.Up {
		requestDirection = "down"
	}
	added := false
	for _, elevator := range es.Elevators {
		if elevator.CanHonorFloorPanelRequest(req) && elevator.On {
			added = elevator.AddFPRequest(req)
			if added {
				fmt.Printf("Request to go %s from floor %d was processed\n", requestDirection, req.Floor)
				return
			}
		}
	}
	// Request wasn't processed, add it to request queue
	fmt.Printf("Request to go %s from floor %d was queued\n", requestDirection, req.Floor)
	es.reqMutex.Lock()
	defer es.reqMutex.Unlock()
	es.RequestQueue = append(es.RequestQueue, req)
}

/**
Notify control unit when a elevator is idle,
it will retry sending requests
*/
func (es *ElevatorSystem) OnIdleElevator(e *Elevator) {
	es.mu.Lock()
	defer es.mu.Unlock()
	if len(es.RequestQueue) > 0 {
		e.AddFPRequest(es.RequestQueue[0])
		es.RequestQueue = es.RequestQueue[1:]
		for len(es.RequestQueue) > 0 {
			go es.ProcessRequest(es.RequestQueue[0])
			es.RequestQueue = es.RequestQueue[1:]
		}
	}
}

/**
Mark request as processed for floor panel
*/
func (es *ElevatorSystem) ReqFinished(floor uint, up bool) {
	if es.FpMapper[floor] != nil {
		es.FpMapper[floor].FulfillRequest(up)
	}
}

/**
Get floor panel
*/
func (es *ElevatorSystem) GetFloorPanel(floor uint) *FloorPanel {
	return es.FpMapper[floor]
}

/**
Get floor panel
*/
func (es *ElevatorSystem) GetElevator(id uint) *Elevator {
	for _, elevator := range es.Elevators {
		if elevator.ID == id {
			return elevator
		}
	}
	return nil
}
