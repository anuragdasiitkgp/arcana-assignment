package elevator

import (
	"time"
)

const DEFAULT_PASSENGER_SYSTEM = "P"
const START_ID = 1
const START_FLOOR = 1
const DEFAULT_INTERVAL = 5 * time.Second
const DEFAULT_DOOR_OPEN = 5 * time.Second

type ControlUnit map[string]*ElevatorSystem

// Singleton
var controlUnit ControlUnit

// Add Type
func AddType(typ string) *ElevatorSystem {
	if controlUnit[typ] != nil {
		return controlUnit[typ]
	}
	elevatorSystem := &ElevatorSystem{
		Type:      typ,
		Elevators: []*Elevator{},
		FpMapper:  make(map[uint]*FloorPanel),
		MinFloor:  9223372036854775807, // int64 max
		MaxFloor:  0,
	}
	controlUnit[typ] = elevatorSystem
	return controlUnit[typ]
}

func Init() {
	if controlUnit == nil {
		controlUnit = make(map[string]*ElevatorSystem)
		elevatorSystem := AddType(DEFAULT_PASSENGER_SYSTEM)
		AddElevator(DEFAULT_PASSENGER_SYSTEM)
		controlUnit[DEFAULT_PASSENGER_SYSTEM] = elevatorSystem
	}
}

func GetControlUnit() ControlUnit {
	return controlUnit
}
