package utils

import (
	"arcana-assignment/packages/elevator"
	"fmt"
)

const (
	INP_CALL_ELEVATOR_TO_GO_UP   = 1
	INP_CALL_ELEVATOR_TO_GO_DOWN = 2
	INP_PRESS_ELEVATOR_BUTTON    = 3
	INP_ELEVATOR_HOLD_DOOR       = 4
	INP_ELEVATOR_STATUS          = 5
	INP_CP_ADD_ELEVATOR          = 6
	INP_CP_ADD_TYPE              = 7
	INP_CP_SWITCH_OFF_ELEVATOR   = 8
	INP_CP_SWITCH_ON_ELEVATOR    = 9
	INP_HELP                     = 10
	INP_CP_ADD_FP                = 11
	INP_EXIT                     = 12
	INP_UP                       = "up"
	INP_DOWN                     = "down"
)

func HandleInput(controlPanel elevator.ControlUnit) {
	menu := []string{
		"Default Elevator Type - P",
		"----- FLOOR PANEL -----",
		"1. Call Elevator To Go Up",
		"2. Call Elevator To Go Down",
		"---- ELEVATOR PANEL ----",
		"3. Enter Floor Number",
		"4. Hold/Close Door",
		"5. Get elevator Status",
		"---- CONTROL PANEL ----",
		"6. Add Elevator",
		"7. Add New Type",
		"8. Turn Off Elevator",
		"9. Turn On Elevator",
		"10. Show Help",
		"11. Add Floor Panel",
		"12. Exit",
	}
	printMenu(menu)
	shouldExit := false
	for !shouldExit {
		var choice uint
		fmt.Println("Enter choice:")
		fmt.Scanf("%d", &choice)
		switch choice {
		case INP_CALL_ELEVATOR_TO_GO_UP:
			processInputForCallElevator(controlPanel, true)
		case INP_CALL_ELEVATOR_TO_GO_DOWN:
			processInputForCallElevator(controlPanel, false)
		case INP_PRESS_ELEVATOR_BUTTON:
			processInputForElevatorButton(controlPanel)
		case INP_HELP:
			printMenu(menu)
		case INP_ELEVATOR_HOLD_DOOR:
			processInputForHoldingDoor(controlPanel)
		case INP_ELEVATOR_STATUS:
			processInputForElevatorStatus(controlPanel)
		case INP_CP_ADD_ELEVATOR:
			processInputForAddingElevator(controlPanel)
		case INP_CP_SWITCH_ON_ELEVATOR:
			processInputForSwitchElevator(controlPanel, true)
		case INP_CP_SWITCH_OFF_ELEVATOR:
			processInputForSwitchElevator(controlPanel, false)
		case INP_CP_ADD_TYPE:
			processInputForAddingType()
		case INP_CP_ADD_FP:
			processInputForAddingFloorPanel(controlPanel)
		case INP_EXIT:
			shouldExit = true
			fmt.Println("Exiting")
		default:
			fmt.Println("Invalid choice")
		}
	}
}

func printMenu(menu []string) {
	for _, item := range menu {
		fmt.Println(item)
	}
}

func processInputForCallElevator(controlPanel elevator.ControlUnit, up bool) {
	var str1Input string
	var num1Input uint
	fmt.Println("Input type <space> floor")
	fmt.Scanf("%s", &str1Input)
	fmt.Scanf("%d", &num1Input)
	if controlPanel[str1Input] == nil {
		fmt.Println("Invalid elevator type")
		return
	}
	typCp := controlPanel[str1Input]
	fp := typCp.GetFloorPanel(num1Input)
	if fp == nil {
		fmt.Println("This floor doesn't have floor panel")
		return
	}
	go fp.CallElevator(up)
}

func processInputForElevatorButton(controlPanel elevator.ControlUnit) {
	var str1Input string
	var num1Input uint
	var num2Input uint
	fmt.Println("Input type <space> elevator id <space> destination")
	fmt.Scanf("%s", &str1Input)
	fmt.Scanf("%d", &num1Input)
	fmt.Scanf("%d", &num2Input)
	if controlPanel[str1Input] == nil {
		fmt.Println("Invalid elevator type")
		return
	}
	typCp := controlPanel[str1Input]
	invokedElevator := typCp.GetElevator(num1Input)
	if invokedElevator == nil {
		fmt.Println("Not a valid elevator id")
		return
	}
	go invokedElevator.PressFloorButton(num2Input)
}

func processInputForSwitchElevator(controlPanel elevator.ControlUnit, on bool) {
	var str1Input string
	var num1Input uint
	fmt.Println("Input type <space> elevator ID")
	fmt.Scanf("%s", &str1Input)
	fmt.Scanf("%d", &num1Input)
	if controlPanel[str1Input] == nil {
		fmt.Println("Invalid elevator type")
		return
	}
	typCp := controlPanel[str1Input]
	elevatorUnit := typCp.GetElevator(num1Input)
	if elevatorUnit == nil {
		fmt.Println("Invalid elevator id")
		return
	}
	if on {
		go elevatorUnit.SwitchOn()
	} else {
		go elevatorUnit.SwitchOff()
	}
}

func processInputForAddingType() {
	var str1Input string
	fmt.Println("Input type")
	fmt.Scanf("%s", &str1Input)
	output := elevator.AddType(str1Input)
	fmt.Printf("%s type added successfully\n", output.Type)
}

func processInputForAddingFloorPanel(controlPanel elevator.ControlUnit) {
	var str1Input string
	var num1Input uint
	fmt.Println("Input type <space> number of floors")
	fmt.Scanf("%s", &str1Input)
	fmt.Scanf("%d", &num1Input)
	if controlPanel[str1Input] == nil {
		fmt.Println("Invalid elevator type")
		return
	}
	fmt.Println("Enter floor numbers:")
	for i := 1; i <= int(num1Input); i++ {
		var floor uint
		fmt.Scanf("%d", &floor)
		go elevator.AddFloorPanel(str1Input, floor)
	}
}

func processInputForAddingElevator(controlPanel elevator.ControlUnit) {
	var str1Input string
	fmt.Println("Input type of elevator")
	fmt.Scanf("%s", &str1Input)
	go elevator.AddElevator(str1Input)
}

func processInputForHoldingDoor(controlPanel elevator.ControlUnit) {
	var str1Input string
	var num1Input uint
	fmt.Println("Input type <space> elevator id")
	fmt.Scanf("%s", &str1Input)
	fmt.Scanf("%d", &num1Input)
	if controlPanel[str1Input] == nil {
		fmt.Println("Invalid elevator type")
		return
	}
	holdElevator := controlPanel[str1Input].GetElevator(num1Input)
	if holdElevator == nil {
		fmt.Println("Invalid elevator type")
		return
	}
	go holdElevator.SwitchHoldDoor()
}

func processInputForElevatorStatus(controlPanel elevator.ControlUnit) {
	var str1Input string
	var num1Input uint
	fmt.Println("Input type <space> elevator id")
	fmt.Scanf("%s", &str1Input)
	fmt.Scanf("%d", &num1Input)
	if controlPanel[str1Input] == nil {
		fmt.Println("Invalid elevator type")
		return
	}
	holdElevator := controlPanel[str1Input].GetElevator(num1Input)
	if holdElevator == nil {
		fmt.Println("Invalid elevator type")
		return
	}
	fmt.Printf("Elevator %d is on floor %d and status : %d\n", holdElevator.ID, holdElevator.CurrentFloor, holdElevator.Status)
}
