package main

import (
	"arcana-assignment/packages/elevator"
	"arcana-assignment/packages/utils"
)

func main() {
	elevator.Init()
	controlUnit := elevator.GetControlUnit()
	utils.HandleInput(controlUnit)
}
