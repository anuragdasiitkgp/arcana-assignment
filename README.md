# Arcana Elevator Assignment

Run the project using `go run main.go`. You will be displayed with a list of options to perform with the elevator module.

# Functionalities
Implemented an elevator module which consists of the following functionalities:
1. Call elevator from a floor to go up
2. Call elevator from a floor to go down
3. Press button inside elevator to go to a specific floor
4. Hold Or Close elevator door
5. Add a new elevator to handle requests
6. Add a new type of elevator (1 'P' type of elevator for passengers is added by default)
7. Add new types of elevators
8. Switch On/Off Elevator
9. Add Floor Panels

## Models Implemented
1. **Elevator** - The elevator model. Keeps track of elevator direction and stops
2. **FloorPanel** - The panel to invoke elevator on each floor
3. **Request** - Request made from a floor panel to go up or down
4. **ElevatorSystem** - The control unit for a particular system (passenger or service or anything else)
5. **ControlUnit** - Map of string types to elevator system

## Behaviour
The behaviour of the system is as outlined below. This can be tested by running `go test` inside `packages/elevator` **directory** of the project
- New Elevator should be created successfully
- New Elevator Type should be created successfully
- Floor Panel should be created successfully
- If there are idle elevators, requests will be immediately processed
- If there are no idle elevators, requests will be pushed to queue or if possible processed
- Elevator request via floor panel should get satisfied
- Elevator request via elevator panel should get satisfied
- Whenever elevators, get free request will be pushed from queue to elevator
- Condition for queue push:
    - Elevator is going down, but request is for up
    - Elevator is going down, but request to go down is made from floor above elevator floor
    - Elevator is going up, but request is for down
    - Elevator is going up, but request to go up is made from floor below elevator floor
- Condition for immediate processing
    - Elevator is going down, request for down is made from floor below elevator floor
    - Elevator is going up, request to go up is made from floor above elevator floor